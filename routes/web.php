<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::middleware(['auth', 'throttle'])->group(function () {
    Route::get('lang/{locale}', 'DashboardController@lang');

    Route::get('/', 'DashboardController@index');

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


    Route::resource('roles', 'RoleController');

    Route::resource('users', 'UserController');
    Route::get('my-profile','UserController@my_profile')->name('my-profile');

    Route::resource('role-permissions', 'RolePermissionController');
    Route::get('all_module_permissions','RolePermissionController@all_module_permissions')->name('get_all_module_permissions');
    Route::get('user_permissions/{user_id}','RolePermissionController@user_permissions')->name('user_permissions');

    Route::resource('companies', 'CompanyController');

});
