<?php
    return [
        'dashboard' => 'Instrumententafel',
        'access_control' => 'Zugangskontrolle',
        'Roles'=> 'Rollen',
        'Users'=> "Benutzer",
        'Role-Permissions' => "Rollenberechtigungen",
        'edit_profile' => 'Profil bearbeiten',
        'logout' => 'Ausloggen'
    ]
?>
