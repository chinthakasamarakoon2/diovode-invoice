<?php
return [
    'dashboard' => 'Dashboard',
    'access_control' => 'Access Control',
    'Roles'=> 'Roles',
    'Users'=> "Users",
    'Role-Permissions' => "Role Permissions",
    'edit_profile' => 'Edit Profile',
    'logout' => 'Log Out'
]
?>
