@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2 ">
          <h3 class="content-header-title mb-0 d-inline-block">@lang('nav.dashboard')</h3>

        </div>

      </div>
      {{--  <div class="content-detached">  --}}
        <div class="content-body">
            <section class="row">
            </section>
        </div>
      {{--  </div>   --}}
      {{--  end of the .content-right  --}}
      </div>
    </div>
  </div>
@endsection
