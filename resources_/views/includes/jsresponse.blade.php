<div class="row" ng-cloak>
    <div ng-if='show_success' class="alert alert-success alert-dismissible mb-2 col-md-12" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>@{{show_success_message}}</strong>
    </div>

    <div ng-if='show_error' class="alert alert-danger alert-dismissible mb-2 col-md-12" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>@{{show_error_message}}</strong>
    </div>

</div>
