@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">My Profile</h3>

        </div>
      </div>
        <div class="content-body" ng-controller="UserController">
            <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">

                                <input type="hidden" ng-init='form_data={{Auth::User()}}; create_url="{{action('UserController@update',Auth::User()->id)}}"'>

                            <div class="card-content collapse show">
                                <div class="card-body border-top-blue-grey border-top-lighten-5">
                                    <form class="form">
                                        <div class="form-body">
                                            @include('includes.jsresponse')
                                            @include('includes.loader')
                                            <div class="row">

                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="name">{{__('profile.name')}} <span style="color:red">*</span></label>
                                                        <input type="text" id="name" class="form-control" name="name" ng-model="form_data.name">
                                                        {{--  <input type="text" id="name" class="form-control" name="name" value="{{old('name')?old('name'):(isset($data)?$data->name:'')}}">  --}}
                                                    </div>
                                                </div>

                                                <div class="col-md-8">

                                                    <div class="form-group dissabled">
                                                        <label for="emaildissable">{{__('profile.email')}}</label>
                                                        <input type="text" class="form-control" id="emaildissable" name="emaildissable" value="{{Auth::User()->email}}" disabled>
                                                    </div>
                                                    {{-- <input type="hidden" class="form-control" id="email" name="email" ng-model="form_data.email"> --}}

                                                </div> <!-- end .col-md-4 -->
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="password">{{__('profile.password')}} </label>
                                                    <input type="password" ng-change="validate_password()" class="form-control" id="password" name="password" ng-model='form_data.password' title="Password lenth shoud greather than 8 characters and should contain at least one special character,number, simple letter, capital letter.">

                                                    <span ng-cloak ng-if='show_password_success' class="glyphicon text-success glyphicon-ok form-control-feedback"></span>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-8 no-padding">
                                                        <div class="progress progress-hairline" style="height:6px">
                                                            <div class="progress-bar @{{progress_class}}" style="width:@{{progress_width}}"></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 no-padding text-left" ng-cloak>
                                                        <span class="text-info text-sm pull-left" style="padding-left:15px">@{{pwd_validate_msg}}</span>
                                                    </div>
                                                </div>

                                            </div> <!-- end .col-md-4 -->
                                            <div class="col-md-8">
                                                <div class="form-group" ng-class='_conf_class' >
                                                    <label for="password_confirmation">{{__('profile.password_confirm')}} </label>
                                                    <input type="password" ng-change='confirm_pass()' class="form-control" id="password_confirmation" name="password_confirmation" ng-model='form_data.password_confirmation'>

                                                    <span ng-cloak ng-if='_conf_icon_has_erro' class="glyphicon text-error @{{conf_icon}} form-control-feedback"></span>
                                                    {{-- <span ng-cloak ng-if='_conf_icon_has_sucsess' class="glyphicon text-success glyphicon-ok form-control-feedback"></span> --}}
                                                </div>
                                            </div> <!-- end .col-md-4 -->




                                            </div>
                                        </div>

                                        <div class="form-actions">

                                            <button type="button" ng-click="submitData()" class="btn btn-success">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div> <!-- end the .content-body -->
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{url('template-assets/vendors/js/jquery/jquery-1.11.2.min.js')}}"></script>
  <script src="{{url('template-assets/vendors/js/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
  {{--  <script src="{{url('template-assets/vendors/js/bootstrap/bootstrap.min.js')}}"></script>  --}}
  <script src="{{ url('template-assets/vendors/js/angular/angular.min.js') }} "></script>
  {{--  <script data-require="angular.js@1.6.5" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>  --}}

  <script src="{{ url('angularControllers/appDeclare/basic.js') }} "></script>
  <script src="{{ url('angularControllers/UserController.js') }} "></script>
@endsection
