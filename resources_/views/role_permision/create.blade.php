@extends('layouts.app')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2">
                    <h3 class="content-header-title mb-0 d-inline-block">Role Permissions - Edit</h3>

                </div>
            </div>
            <div class="content-body" ng-controller="UserPermissionController" ng-cloak>
                <section>
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">

                                <input type="hidden" ng-init='
                                get_all_module_url="{{route('get_all_module_permissions')}}";
                                get_user_permission_url="{{action('RolePermissionController@user_permissions',$role_id)}}";
                                getAllModulePermission();
                            '>

                                <div class="card-content collapse show">
                                    <div class="card-body border-top-blue-grey border-top-lighten-5">
                                        <form class="form">
                                            <div class="form-body">
                                                @include('includes.jsresponse')
                                                @include('includes.loader')

                                                <div class="row skin skin-square">



                                                    <ul class="list" ng-cloak>
                                                        <li class="divider-full-bleed tile" ng-repeat = "module in modules">
                                                            <div class="tile-content ink-reaction">

                                                                <div class="tile-text">
                                                                    @{{module.name}}

                                                                    <small>
                                                                        <div class="checkbox checkbox-styled" style="padding-left:30px" ng-repeat = "permission in module.permission">
                                                                            <label>
                                                                                <input type="checkbox" ng-model = 'user_permission[permission.id]'>
                                                                                    <span>
                                                                                    @{{permission.name}}
                                                                                    </span>
                                                                            </label>
                                                                        </div>
                                                                    </small>
                                                                </div>
                                                            </div>

                                                        </li>

                                                        <!-- <li class="divider-full-bleed"></li> -->
                                                    </ul>

                                                    <input type="hidden" ng-init='create_url="{{action('RolePermissionController@store')}}"; role_id="{{$role_id}}"'>

                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <button type="button" ng-click="submitData()" class="btn btn-success">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> <!-- end the .content-body -->
        </div>
    </div>
    </div>



@section('scripts')
    <script src="{{url('template-assets/vendors/js/jquery/jquery-1.11.2.min.js')}}"></script>
    <script src="{{url('template-assets/vendors/js/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
    {{--  <script src="{{url('template-assets/vendors/js/bootstrap/bootstrap.min.js')}}"></script>  --}}
    <script src="{{ url('template-assets/vendors/js/angular/angular.min.js') }} "></script>
    {{--  <script data-require="angular.js@1.6.5" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>  --}}

    <script src="{{ url('angularControllers/appDeclare/basic.js') }} "></script>
    <script src="{{ url('angularControllers/user_permission-controller.js') }} "></script>
@endsection
