@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
          <h3 class="content-header-title mb-0 d-inline-block">Roles</h3>
          <div class="row breadcrumbs-top d-inline-block">
            <div class="breadcrumb-wrapper col-12">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="">Authentication</a></li>
                <li class="breadcrumb-item"><a href="">Role Permissions</a></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
      {{--  <div class="content-detached">  --}}
        <div class="content-body">
            <section class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-head">
                        <div class="card-header">
                            <h4 class="card-title">All Role Permissions
                                {{-- @lang('lang.welcome') --}}
                            </h4>
                            <a class="heading-elements-toggle"><i class="ft-ellipsis-h font-medium-3"></i></a>
                            <div class="heading-elements">
                                {{-- <a href="{{url('role-permissions/create')}}" class="btn btn-primary btn-sm"><i class="ft-plus white"></i> Add Role</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body border-top-blue-grey border-top-lighten-5">
                            <form action="{{action('RoleController@index')}}">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" id="name" class="form-control" placeholder="Name" name="name" value="{{$name}}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="submit"   class="btn btn-raised btn-info ink-reaction">Search</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                            <hr>

                            @include('includes.massage')
                            <!-- Task List table -->
                            <div class="table-responsive">
                                <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                                    <thead>
                                        <tr>
                                            <th>Serial</th>
                                            <th>Name</th>
                                            <th style="max-width: 100px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td class="text-left">{{$user->name}}</td>
                                                <td>
                                                    <span class="dropdown">
                                                        <button id="btnSearchDrop2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="btn btn-primary dropdown-toggle dropdown-menu-right"><i class="ft-settings"></i></button>
                                                        <span aria-labelledby="btnSearchDrop2" class="dropdown-menu mt-1 dropdown-menu-right">
                                                            <a href="{{action('RolePermissionController@edit',$user->id)}}" class="dropdown-item"><i class="ft-edit-2"></i> Edit</a>
                                                            <a href="#" class="dropdown-item"><i class="ft-trash-2"></i> View</a>
                                                        </span>
                                                    </span>
                                                </td>
                                            </tr>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
      {{--  </div>   --}}
      {{--  end of the .content-right  --}}
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection

@section('scripts')
    <script src="{{ url('template-assets/js/scripts/pages/users-contacts.js') }}"></script>
@endsection
