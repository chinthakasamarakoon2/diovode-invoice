app.controller('UserPermissionController', ['$scope', '$timeout', '$http', 'CSRF_TOKEN', function ($scope, $timeout, $http, CSRF_TOKEN) {

    $scope.show_loader = false;
    $scope.show_success = false;
    $scope.show_success_message = '';
    $scope.show_error = false;
    $scope.show_error_message = '';
    $scope.errors = null;

    $scope.user_permission = {};
    $scope.modules = {}

    $scope.getAllModulePermission = () => {
        $scope.show_loader=true;
        $http.get($scope.get_all_module_url)
            .then((response) => {
                $scope.modules = response.data;
                console.log($scope.modules);
                $scope.show_loader = false;
                $scope.getUserPermission();
            })
            .catch((error)=>{
                console.log(error.data);
                $scope.show_loader = false;
            })
    }

    $scope.getUserPermission = () => {
        $scope.show_loader=true;
        $http.get($scope.get_user_permission_url)
            .then((response) => {
                // $scope.modules = response.data;
                console.log('user : ',response.data);
                angular.forEach(response.data, function (val) {
                    $scope.user_permission[val] = true;
                });
                $scope.show_loader = false;
            })
            .catch((error)=>{
                console.log(error.data);
                $scope.show_loader = false;
            })
    }

    $scope.submitData = () => {
        console.log($scope.user_permission);
        $scope.before_request();
        $http.post($scope.create_url,{'role_id':$scope.role_id, 'permissions':$scope.user_permission})
            .then((response) => { // get the all bookings.
                if (response.data.status) {
                    $scope.show_success = true;
                    $scope.show_error = false;
                    $scope.show_loader = false;
                    $scope.show_success_message = response.data.message;
                    $scope.form_data = {};
                }
            }).catch((error) => {
                $scope.show_success = false;
                $scope.show_error = true;
                $scope.show_loader = false;
                $scope.show_error_message = error.data.message;
                console.log('Error : ', error.data);
            });
    }

    $scope.before_request = () => {
        $scope.show_loader = true;
        $scope.show_success = false;
        $scope.show_error = false;
    }
}]);
