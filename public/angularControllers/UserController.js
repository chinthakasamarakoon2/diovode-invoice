app.controller('UserController', ['$scope', '$timeout', '$http', 'CSRF_TOKEN', function ($scope, $timeout, $http, CSRF_TOKEN) {

    $scope.show_loader = false;
    $scope.show_success = false;
    $scope.show_success_message = '';
    $scope.show_error = false;
    $scope.show_error_message = '';
    $scope.errors = null;
    $scope.progress_width = 0+'%';
    $scope.progress_class = 'progress-bar-default';

    $scope.staff_details = {};



    $scope.submitData = () => {
        $scope.before_request();
        if($scope.action=='New'){
            $http.post($scope.create_url, $scope.form_data)
            .then((response) => { // get the all bookings.
                if (response.data.status) {
                    $scope.show_success = true;
                    $scope.show_error = false;
                    $scope.show_loader = false;
                    $scope.show_success_message = response.data.message;
                    $scope.form_data = {};
                }
            }).catch((error) => {
                $scope.show_success = false;
                $scope.show_error = true;
                $scope.show_loader = false;
                $scope.show_error_message = error.data.message;
                console.log('Error : ', error.data);
            });
        }
        else{
            $http.put($scope.create_url, $scope.form_data)
            .then((response) => { // get the all bookings.
                if (response.data.status) {
                    $scope.show_success = true;
                    $scope.show_error = false;
                    $scope.show_loader = false;
                    $scope.show_success_message = response.data.message;
                }
            }).catch((error) => {
                $scope.show_success = false;
                $scope.show_error = true;
                $scope.show_loader = false;
                $scope.show_error_message = error.data.message;
                console.log('Error : ', error.data);
            });
        }
    }

    $scope.before_request = () => {
        $scope.show_loader = true;
        $scope.show_success = false;
        $scope.show_error = false;
    }

    $scope.validate_password = () =>{
        var marks = 0;
        var password = $scope.form_data.password;
        console.log('password.length : ', password.length);
        if (password.length > 7) { // check the lenth of password
            marks++;
            console.log('con1');
        }
        else{
            if(marks>0){
                // marks--;
            }
        }

        if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)){ // check password has atleast one special character.
            marks++;
            console.log('con2');
        }
        else {
            if (marks > 0) {
                // marks--;
            }
        }
        if (password.match(/\d+/)){ // check password has atleast one special character.
            marks++;
            console.log('con3');
        }
        else {
            if (marks > 0) {
                // marks--;
            }
        }
        if (password.match(/[a-z]/)){ // check password has atleast one special character.
            marks++;
            console.log('con4');
        }
        else {
            if (marks > 0) {
                // marks--;
            }
        }
        if (password.match(/[A-Z]/)) { // check password has atleast one special character.
            marks++;
            console.log('con5');
        }
        else {
            if (marks > 0) {
                // marks--;
            }
        }
        var final_mark = marks/5*100;
        console.log('final_mark :', final_mark);
        $scope.progress_width = final_mark + '%';
        $scope.pwd_validate_msg = '';
        $scope.show_password_success = false;
        $scope.show_password_success_class = '';
        if(final_mark >90){
            $scope.progress_class = 'progress-bar-success';
            $scope.pwd_validate_msg = 'Strong';
            $scope.show_password_success = true;
            $scope.show_password_success_class = 'has-success';
        }
        else if(final_mark >79){
            $scope.progress_class = 'progress-bar-info';
            $scope.pwd_validate_msg = 'Good';
        }
        else if (final_mark > 59) {
            $scope.progress_class = 'progress-primary-bright';
            $scope.pwd_validate_msg = 'Medium';
        }
        else if (final_mark > 39) {
            $scope.progress_class = 'progress-bar-warning';
            $scope.pwd_validate_msg = 'Weak';
        }
        else{
            $scope.progress_class = 'progress-bar-danger';
            $scope.pwd_validate_msg = 'Weak';
        }
    }

    $scope.confirm_pass = () => {
        if ($scope.form_data.password_confirmation.length>0){
            $scope._conf_icon_has_erro = 'true';
            $scope._conf_icon_has_sucsess = 'false';
            $scope._conf_class = 'has-error';
            $scope.conf_icon = 'glyphicon-exclamation-sign';
            if ($scope.form_data.password_confirmation == $scope.form_data.password){
                $scope._conf_class = 'has-success';
                $scope.conf_icon = 'glyphicon-ok';
                $scope._conf_icon_has_erro = 'false';
                $scope._conf_icon_has_sucsess = 'true';
            }
        }
    }

    // $scope.uplord_img = () =>

}]);
