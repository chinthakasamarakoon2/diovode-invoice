app.controller('public-controller', ['$scope', '$timeout', '$http', 'CSRF_TOKEN', function ($scope, $timeout, $http, CSRF_TOKEN) {

    $scope.show_loader = false;
    $scope.show_success = false;
    $scope.show_success_message = '';
    $scope.show_error = false;
    $scope.show_error_message = '';
    $scope.errors = null;

    $scope.submitData = () => {
        console.log('Okkk');
        $scope.before_request();
        if($scope.action=='New'){
            $http.post($scope.create_url, $scope.form_data)
            .then((response) => { // get the all bookings.
                if (response.data.status) {
                    $scope.show_success = true;
                    $scope.show_error = false;
                    $scope.show_loader = false;
                    $scope.show_success_message = response.data.message;
                    $scope.form_data = {};
                }
            }).catch((error) => {
                $scope.show_success = false;
                $scope.show_error = true;
                $scope.show_loader = false;
                $scope.show_error_message = error.data.message;
                console.log('Error : ', error.data);
            });
        }
        else{
            $http.put($scope.create_url, $scope.form_data)
            .then((response) => { // get the all bookings.
                if (response.data.status) {
                    $scope.show_success = true;
                    $scope.show_error = false;
                    $scope.show_loader = false;
                    $scope.show_success_message = response.data.message;
                }
            }).catch((error) => {
                $scope.show_success = false;
                $scope.show_error = true;
                $scope.show_loader = false;
                $scope.show_error_message = error.data.message;
                console.log('Error : ', error.data);
            });
        }
    }

    $scope.before_request = () => {
        $scope.show_loader = true;
        $scope.show_success = false;
        $scope.show_error = false;
    }
}]);
