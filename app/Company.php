<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'name',
        'legal_name',
        'company_holder',
        'industry',
        'type_of_company',
        'street',
        'zip_code',
        'city',
        'country',
        'district_court',
        'co_reg_number',
        'sales_tax_id',
        'tax_ref',
        'tax_rate',
        'phone',
        'fax',
        'email',
        'web',
        'bank',
        'acount_number',
        'company_logo',
        'active'
    ];
}
