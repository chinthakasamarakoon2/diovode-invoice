<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'legal_name' =>'nullable|max:255',
            'company_holder' => 'nullable|max:255',
            'industry' =>'nullable|max:255',
            'type_of_company'  =>'nullable|max:255',
            'street'  =>'nullable|max:255',
            'zip_code' =>'nullable|max:255',
            'city' =>'nullable|max:255',
            'country' =>'nullable|max:255',
            'district_court' =>'nullable|max:255',
            'co_reg_number' =>'nullable|max:255',
            'sales_tax_id' =>'nullable|max:255',
            'tax_ref' =>'nullable|max:255',
            'tax_rate' =>'nullable|numeric|numeric',
            'phone' =>'nullable|max:255',
            'fax' =>'nullable|max:255',
            'email' =>'nullable|email|max:255',
            'web' =>'nullable|max:255',
            'bank' =>'nullable|max:255',
            'acount_number' =>'nullable|max:255',

            'user_email' => "required|email|unique:users,name|max:100",
            'password' => "required|confirmed|min:8",
        ];
    }
}
