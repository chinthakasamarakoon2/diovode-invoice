<?php

namespace App\Http\Controllers;

use App\Company;
use App\Setting;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use App\User;
use App\UserDetails;
use Illuminate\Support\Facades\Hash;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::orderby('id','desc')->get();
        return view('company.index',compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = "New";
        $company_type = Setting::where('group','type-of-company')->get();
        $company_industry = Setting::where('group','company-industry')->get();
        $countries = Country::where('active',1)->get();
        return view('company.create',compact('action','company_type','countries','company_industry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        try{
            $_company = new Company();
            $_company->fill($request->all());
            $_company->save();

            // create the user acount

            $_user = new User();
            $_user->email = $request['user_email'];
            $_user->name = $request['user_name'];
            $_user->company_id = $_company->id;
            $_user->role_id = 8;
            $_user->password = Hash::make($request['password']);
            $_user->active = 1;
            $_user->save();
            $user_details = new UserDetails();
            $user_details->user_id = $_user->id;
            $user_details->save();

            return response()->json(['status'=>true, 'message' => 'Company and User has been created successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>true, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Company::find($id);
        return view('company.view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $action = "Edit";
        $data = Company::find($id);
        $company_type = Setting::where('group','type-of-company')->get();
        $company_industry = Setting::where('group','company-industry')->get();
        $countries = Country::where('active',1)->get();
        return view('company.create',compact('action','company_type','countries','company_industry','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $_company = Company::find($id);
            $_company->fill($request->all());
            $_company->save();

            // create the user acount

            // $_user = new User();
            // $_user->email = $request['user_email'];
            // $_user->name = $request['user_name'];
            // $_user->company_id = $_company->id;
            // $_user->role_id = 8;
            // $_user->password = Hash::make($request['password']);
            // $_user->active = 1;
            // $_user->save();
            // $user_details = new UserDetails();
            // $user_details->user_id = $_user->id;
            // $user_details->save();

            return response()->json(['status'=>true, 'message' => 'Company and User has been updated successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>true, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
