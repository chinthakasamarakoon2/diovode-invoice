<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use DB;
use App;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request['name'];
        $roles = Role::orderby('id','desc')
                ->when(!empty($name),function($q) use($name){
                    $q->where('name','LIKE','%'.$name.'%');
                })
                ->get();

        return view('roles.index',compact('roles','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = "New";
        return view('roles.create',compact('action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        try{
            $_role = new Role();
            $_role->fill($request->all());
            $_role->save();
            return response()->json(['status'=>true, 'message' => 'Role has been created successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>true, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Role::find($id);
        $action = "Edit";
        return view('roles.create',compact('action','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $_role = Role::find($id);
            $_role->fill($request->all());
            $_role->save();
            return response()->json(['status'=>true, 'message' => 'Role has been updated successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>true, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $role = Role::find($id)->delete();
            return redirect(action('RoleController@index'))->with(['success'=> 'Role id '.$id.' has been susccessfully deleted.']);
        }
        catch(\Exception $e){
            return redirect(action('RoleController@index'))->with(['error', 'Sorry!Something went wrong.']);
        }
    }
}
