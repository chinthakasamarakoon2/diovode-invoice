<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class DashboardController extends Controller
{

    public function index()
    {
        return view('dashboard');
    }

    public function lang($locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }
}
