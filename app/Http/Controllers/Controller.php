<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct()
    {
        // $this->middleware('isActive')->except(['viewLogin', 'login']);   //check user is active
        $this->middleware('auth')->except(['viewLogin', 'login', 'logout']); // all function excepts viewLogin and login should be authenticated to access
        // $this->middleware('guest')->only(['viewLogin', 'login']); // show if user is guest.

    }
}
