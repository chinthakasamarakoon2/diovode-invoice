<?php

namespace App\Http\Controllers;

use App\UserDetails;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request['name'];
        $users = User::with('details','role')->get();
        // dd($users);
        return view('users.index',compact('users','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action="New";
        $roles = Role::where('active',1)->get();
        return view('users.create',compact('roles','action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try{
            $_user = new User();
            $_user->fill($request->all());
            $_user->password = Hash::make($request['password']);
            $_user->active = 1;
            $_user->save();
            return response()->json(['status'=>true, 'message' => 'User has been created successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>false, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function show(UserDetails $userDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(UserDetails $userDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        try{
            $_user =  User::find($id);
            $_user->fill($request->all());
            $_user->password = Hash::make($request['password']);
            $_user->active = 1;
            $_user->save();
            return response()->json(['status'=>true, 'message' => 'User has been updated successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>false, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDetails $userDetails)
    {
        //
    }
}
