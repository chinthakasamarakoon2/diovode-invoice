<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;


class ForgotPasswordController extends Controller
{
    protected $redirectTo = RouteServiceProvider::HOME;

    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest');
    }
}
