<?php

namespace App\Http\Controllers;

use App\RolePermission;
use App\User;
use App\Role;
use App\Module;
use Illuminate\Http\Request;

class RolePermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = $request['name'];
        $users = Role::where('active',1)
            ->orderby('id','desc')
            ->when(!empty($name),function($q) use($name){
                $q->where('name','LIKE','%'.$name.'%');
            })
            ->get();

        return view('role_permision.index',compact('users','name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $role_id = $request['role_id'];
            RolePermission::where('role_id',$role_id)->delete();//delete the old permissions
            foreach ($request['permissions'] as $key => $val) {
                if($val){
                    $user_permission = new RolePermission();
                    $user_permission->role_id = $role_id;
                    $user_permission->module_function_id  = $key;
                    $user_permission->save();
                }
            }
            return response()->json(['status'=>true, 'message' => 'Role permission has been modified successfully.'],200);
        }
        catch(\Exception $ex){
            return response()->json(['status'=>false, 'message' => 'Something went wrong.try again later.'],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function show(RolePermission $rolePermission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function edit($role_id)
    {
        $role_id = $role_id;
        return view('role_permision.create', compact('role_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RolePermission $rolePermission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RolePermission  $rolePermission
     * @return \Illuminate\Http\Response
     */
    public function destroy(RolePermission $rolePermission)
    {
        //
    }

    // return the all permissions
    public function all_module_permissions(){
        $module = Module::with('permission')->where('active',1)->get();
        return response()->json($module,200);
    }

    // return the user permissions
    public function user_permissions($role_id ){
        $u_permi = RolePermission::where('role_id',$role_id )->pluck('module_function_id')->toArray();
        return response()->json($u_permi,200);
    }
}
