<?php

namespace App\Http\Controllers;

use App\ModuleFunctions;
use Illuminate\Http\Request;

class ModuleFunctionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ModuleFunctions  $moduleFunctions
     * @return \Illuminate\Http\Response
     */
    public function show(ModuleFunctions $moduleFunctions)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ModuleFunctions  $moduleFunctions
     * @return \Illuminate\Http\Response
     */
    public function edit(ModuleFunctions $moduleFunctions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ModuleFunctions  $moduleFunctions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ModuleFunctions $moduleFunctions)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ModuleFunctions  $moduleFunctions
     * @return \Illuminate\Http\Response
     */
    public function destroy(ModuleFunctions $moduleFunctions)
    {
        //
    }
}
