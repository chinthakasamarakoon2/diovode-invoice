{{--  <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>  --}}

<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
    {{-- <html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr"> --}}
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">

    <title>Invoice Manager</title>
    <link rel="apple-touch-icon" href="{{ url('template-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('template-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CMuli:300,400,500,700" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/css/vendors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/vendors/css/tables/extensions/rowReorder.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/vendors/css/tables/extensions/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/vendors/css/forms/icheck/icheck.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/vendors/css/forms/icheck/custom.css') }}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/css/app.css') }}">
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/css/core/colors/palette-gradient.css') }}">
    {{--  <link rel="stylesheet" type="text/css" href="{{ url('template-assets/css/pages/users.css') }}">  --}}
    @yield('styles')
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('template-assets/assets/css/style.css') }}">
    <!-- END Custom CSS-->

    <style>
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }

        .loader {
            margin: 0 auto;
            width: 60px;
            height: 50px;
            text-align: center;
            font-size: 10px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translateY(-50%) translateX(-50%);
            z-index: 999;
        }

        .loader>div {
            height: 100%;
            width: 8px;
            display: inline-block;
            float: left;
            margin-left: 2px;
            -webkit-animation: delay 0.8s infinite ease-in-out;
            animation: delay 0.8s infinite ease-in-out;
        }

        .loader .bar1 {
            background-color: #754fa0;
        }

        .loader .bar2 {
            background-color: #09b7bf;
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s;
        }

        .loader .bar3 {
            background-color: #90d36b;
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s;
        }

        .loader .bar4 {
            background-color: #f2d40d;
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s;
        }

        .loader .bar5 {
            background-color: #fcb12b;
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s;
        }

        .loader .bar6 {
            background-color: #ed1b72;
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s;
        }

        @-webkit-keyframes delay {

            0%,
            40%,
            100% {
                -webkit-transform: scaleY(0.05);
            }

            20% {
                -webkit-transform: scaleY(1);
            }
        }

        @keyframes delay {

            0%,
            40%,
            100% {
                 transform: scaleY(0.05);
                -webkit-transform: scaleY(0.05);
            }

            20% {
                transform: scaleY(1);
                -webkit-transform: scaleY(1);
            }
        }

    </style>


  </head>
  <body  ng-app="diovode-inv" class="vertical-layout vertical-menu content-detached-left-sidebar   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="content-detached-left-sidebar">

    @yield('content')

    {{--  <footer class="footer footer-static footer-light navbar-border">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright  &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT </a>, All rights reserved. </span><span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span></p>
      </footer>  --}}

      <!-- BEGIN VENDOR JS-->
      <script src="{{ url('template-assets/vendors/js/vendors.min.js') }}"></script>
      <!-- BEGIN VENDOR JS-->
      <!-- BEGIN PAGE VENDOR JS-->
      <script src="{{ url('template-assets/vendors/js/tables/jquery.dataTables.min.js') }}"></script>
      <script src="{{ url('template-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
      <script src="{{ url('template-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
      <script src="{{ url('template-assets/vendors/js/tables/datatable/dataTables.rowReorder.min.js') }}"></script>
      <script src="{{ url('template-assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
      <!-- END PAGE VENDOR JS-->
      <!-- BEGIN ROBUST JS-->
      <script src="{{ url('template-assets/js/core/app-menu.js') }}"></script>
      <script src="{{ url('template-assets/js/core/app.js') }}"></script>
      <!-- END ROBUST JS-->
      <!-- BEGIN PAGE LEVEL JS-->
      @yield('scripts')
      {{--  <script src="{{ url('template-assets/js/scripts/pages/users-contacts.js') }}"></script>  --}}
  </body>
</html>
