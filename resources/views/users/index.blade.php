@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Users</h3>

        </div>

          <div class="col-md-4">
              <a href="{{url('users/create')}}" class="btn btn-success btn-sm pull-right"><i class="ft-plus white"></i> Add User</a>
          </div>

      </div>
      {{--  <div class="content-detached">  --}}
        <div class="content-body">
            <section class="row">
            <div class="col-12">

                <div class="card">

                    <div class="card-content">
                        <div class="card-body border-top-blue-grey border-top-lighten-5">


                            @include('includes.massage')
                            <!-- Task List table -->
                            <div class="table-responsive">
                                <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role</th>
                                            <th>Phone</th>
                                            <th>Active</th>
                                            <th style="max-width: 100px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($users as $user)
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td class="text-left">{{$user->name}}</td>
                                                <td class="text-left">{{$user->email}}</td>
                                                <td class="text-left">{{$user->role->name}}</td>
                                                <td class="text-left">{{$user->details->phone}}</td>
                                                <td class="text-center">
                                                    @if($user->active)
                                                        <span class="badge badge-success">Yes</span>
                                                    @else
                                                    <span class="badge badge-danger">No</span>
                                                    @endif
                                                    {{--  <input type="checkbox" @if($user->active) checked @endif class="input-chk">  --}}
                                                </td>
                                                <td>
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <a href="{{action('UserController@edit',$user->id)}}" class="btn btn-icon btn-info btn-sm"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
      {{--  </div>   --}}
      {{--  end of the .content-right  --}}
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection

@section('scripts')
    <script src="{{ url('template-assets/js/scripts/pages/users-contacts.js') }}"></script>
@endsection
