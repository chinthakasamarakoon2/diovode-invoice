@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Roles - {{$action}}</h3>

        </div>
      </div>
        <div class="content-body" ng-controller="public-controller">
            <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">

                            @if($action=='New')
                                <input type="hidden" ng-init='create_url="{{action('RoleController@store')}}"; action="{{$action}}"'>
                            @else
                                <input type="hidden" ng-init='form_data={{$data}}; create_url="{{action('RoleController@update',$data->id)}}"; action="{{$action}}"'>
                            @endif

                            <div class="card-content collapse show">
                                <div class="card-body border-top-blue-grey border-top-lighten-5">
                                    <form class="form">
                                        <div class="form-body">
                                            @include('includes.jsresponse')
                                            @include('includes.loader')
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Role Name</label>
                                                        <input type="text" id="name" class="form-control" name="name" ng-model="form_data.name">
                                                        {{--  <input type="text" id="name" class="form-control" name="name" value="{{old('name')?old('name'):(isset($data)?$data->name:'')}}">  --}}
                                                    </div>
                                                </div>
                                                @if ($action =="Edit")
                                                    <div class="col-md-6"><br></br>

                                                        <input class="" type="checkbox"  class="input-chk" name="active" ng-model="form_data.active" ng-true-value="1" ng-false-value="0">
                                                        <label for="active">Active</label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <button type="button" ng-click="submitData()" class="btn btn-success">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                            @if ($action =="Edit")
                                            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#danger">
                                                <i class="fa fa-times"></i> Delete
                                            </button>
                                            @endif

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div> <!-- end the .content-body -->
      </div>
    </div>
  </div>

  <!-- Modal -->
  @if ($action =="Edit")
    <div class="modal fade text-left" id="danger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
      <div class="modal-dialog" role="document">
    	<div class="modal-content">
    	  <div class="modal-header bg-danger white">
    		<h4 class="modal-title white" id="myModalLabel10">Confirmation</h4>
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    		  <span aria-hidden="true">&times;</span>
    		</button>
    	  </div>
    	  <div class="modal-body">
    		<h5>Do you want to delete the role {{$data->name}} from roles?</h5>
    	  </div>
    	  <div class="modal-footer">
            <form action="{{action('RoleController@destroy',$data->id)}}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
    		<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline-danger">Delete</button>
            </form>
    	  </div>
    	</div>
      </div>
    </div>
    @endif
@endsection

@section('scripts')
  <script src="{{url('template-assets/vendors/js/jquery/jquery-1.11.2.min.js')}}"></script>
  <script src="{{url('template-assets/vendors/js/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
  {{--  <script src="{{url('template-assets/vendors/js/bootstrap/bootstrap.min.js')}}"></script>  --}}
  <script src="{{ url('template-assets/vendors/js/angular/angular.min.js') }} "></script>
  {{--  <script data-require="angular.js@1.6.5" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>  --}}

  <script src="{{ url('angularControllers/appDeclare/basic.js') }} "></script>
  <script src="{{ url('angularControllers/public-controller.js') }} "></script>
@endsection
