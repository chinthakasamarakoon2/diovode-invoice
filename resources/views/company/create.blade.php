@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Comapny - {{$action}}</h3>

        </div>
      </div>
        <div class="content-body" ng-controller="public-controller">
            <section>
                <div class="row match-height">
                    <div class="col-md-12">
                        <div class="card">

                            @if($action=='New')
                                <input type="hidden" ng-init='create_url="{{action('CompanyController@store')}}"; action="{{$action}}"'>
                            @else
                                <input type="hidden" ng-init='form_data={{$data}}; create_url="{{action('CompanyController@update',$data->id)}}"; action="{{$action}}"'>
                            @endif

                            <div class="card-content collapse show">
                                <div class="card-body border-top-blue-grey border-top-lighten-5">
                                    <form class="form form-horizontal">
                                        <div class="form-body">
                                            @include('includes.jsresponse')
                                            @include('includes.loader')

                                            <h4 class="form-section"><i class="ft-user"></i> {{__('company.address')}}</h4>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.company_name')}}</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.name" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.legal_company_name')}}</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.legal_name" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.company_holder')}}</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.company_holder" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.industry')}}</label>
                                                <div class="col-md-4">
                                                    <select type="text" class="form-control select2" id="industry" name="industry" ng-model="form_data.industry" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">
                                                        <option value="">&nbsp;</option>
                                                        @foreach ($company_industry as $type)
                                                            <option ng-value="{{$type->key}}">{{$type->value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.type_of_company')}}</label>
                                                <div class="col-md-4">
                                                    <select type="text" class="form-control select2" id="type_of_company" name="type_of_company" ng-model="form_data.type_of_company" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">
                                                        <option value="">&nbsp;</option>
                                                        @foreach ($company_type as $type)
                                                            <option ng-value="{{$type->key}}">{{$type->value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.address')}}</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="projectinput1" placeholder="Street" class="form-control" ng-model="form_data.street"  name="fname">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1"></label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" placeholder="Zip Code" class="form-control" ng-model="form_data.zip_code" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1"></label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" placeholder="City" class="form-control" ng-model="form_data.city" name="fname">
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">Country</label>
                                                <div class="col-md-4">
                                                    <select type="text" class="form-control select2" id="country" name="country" ng-model="form_data.country" data-toggle="tooltip" data-placement="bottom" data-trigger="hover">
                                                        <option value="">&nbsp;</option>
                                                        @foreach ($countries as $cn)
                                                            <option ng-value="{{$cn->code}}">{{$cn->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <h4 class="form-section"><i class="ft-file"></i> {{__('company.tax-and-register-numbers')}}</h4>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.district-court')}}</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.district_court" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.corporation-registration-number')}}</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.co_reg_number" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.sales-tax-id')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.sales_tax_id" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.tax-reference')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.tax_ref" name="fname">
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.tax-rate')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.tax_rate" name="fname">
                                                </div>
                                            </div>

                                            <h4 class="form-section"><i class="ft-file"></i> {{__('company.contact-information')}}</h4>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.phone')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.phone" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.fax')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.fax" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.email')}}</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.email" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.web')}}</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.web"  name="fname">
                                                </div>
                                            </div>

                                            <h4 class="form-section"><i class="ft-file"></i> {{__('company.payment-information')}}</h4>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.bank')}}</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.bank" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.account-number')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.acount_number" name="fname">
                                                </div>
                                            </div>

                                            <h4 class="form-section"><i class="ft-user"></i> {{__('company.admin-account-details')}}</h4>


                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.email')}}</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.user_email" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.name')}}</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="projectinput1" class="form-control" ng-model="form_data.user_name" name="fname">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.password')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" name="password" ng-model='form_data.password' title="Password lenth shoud greather than 8 characters and should contain at least one special character,number, simple letter, capital letter.">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1">{{__('company.confirm_password')}}</label>
                                                <div class="col-md-4">
                                                    <input type="text" id="projectinput1" class="form-control" id="password_confirmation" name="password_confirmation" ng-model='form_data.password_confirmation'>
                                                </div>
                                            </div>


                                                @if ($action =="Edit")

                                                    <div class="form-group row">
                                                <label class="col-md-3 label-control" for="projectinput1"></label>
                                                        <div class="col-md-9">
                                                            <input class="" type="checkbox"  class="input-chk" name="active" ng-model="form_data.active" ng-true-value="1" ng-false-value="0">
                                                            <label for="active">&nbsp; Active</label>
                                                        </div>
                                                    </div>

                                                @endif
                                        </div>

                                        <div class="form-actions">

                                            <button type="button" ng-click="submitData()" class="btn btn-success">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                            @if ($action =="Edit")
                                            <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#danger">
                                                <i class="fa fa-times"></i> Delete
                                            </button>
                                            @endif
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div> <!-- end the .content-body -->
      </div>
    </div>
  </div>

  <!-- Modal -->
  @if ($action =="Edit")
    <div class="modal fade text-left" id="danger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel10" aria-hidden="true">
      <div class="modal-dialog" role="document">
    	<div class="modal-content">
    	  <div class="modal-header bg-danger white">
    		<h4 class="modal-title white" id="myModalLabel10">Confirmation</h4>
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    		  <span aria-hidden="true">&times;</span>
    		</button>
    	  </div>
    	  <div class="modal-body">
    		<h5>Do you want to delete the company {{$data->name}} from companies?</h5>
    	  </div>
    	  <div class="modal-footer">
            <form action="{{action('RoleController@destroy',$data->id)}}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
    		<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-outline-danger">Delete</button>
            </form>
    	  </div>
    	</div>
      </div>
    </div>
    @endif
@endsection

@section('scripts')
  <script src="{{url('template-assets/vendors/js/jquery/jquery-1.11.2.min.js')}}"></script>
  <script src="{{url('template-assets/vendors/js/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
  {{--  <script src="{{url('template-assets/vendors/js/bootstrap/bootstrap.min.js')}}"></script>  --}}
  <script src="{{ url('template-assets/vendors/js/angular/angular.min.js') }} "></script>
  {{--  <script data-require="angular.js@1.6.5" data-semver="1.6.5" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.js"></script>  --}}

  <script src="{{ url('angularControllers/appDeclare/basic.js') }} "></script>
  <script src="{{ url('angularControllers/public-controller.js') }} "></script>
@endsection
