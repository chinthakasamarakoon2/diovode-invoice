@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Company Profile</h3>

        </div>

      </div>
      {{--  <div class="content-detached">  --}}
        <div class="content-body">
            <section class="row">
            <div class="col-12">

                <div class="card">

                    <div class="card-content">
                        <div class="card-body border-top-blue-grey border-top-lighten-5">

                            @include('includes.massage')

                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                <a class="nav-link active" id="homeIcon-tab" data-toggle="tab" href="#tab1" aria-controls="homeIcon" aria-expanded="true"><i class="ft-user"></i> {{__('company.address')}}</a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" id="profileIcon-tab" data-toggle="tab" href="#tab2" aria-controls="profileIcon" aria-expanded="false"><i class="ft-file"></i> {{__('company.tax-and-register-numbers')}}</a>
                                </li>

                                <li class="nav-item">
                                <a class="nav-link" id="aboutIcon-tab" data-toggle="tab" href="#tab3" aria-controls="about" aria-expanded="false"><i class="ft-file"></i> {{__('company.contact-information')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="aboutIcon-tab1" data-toggle="tab" href="#tab4" aria-controls="about" aria-expanded="false"><i class="ft-file"></i> {{__('company.payment-information')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="aboutIcon-tab2" data-toggle="tab" href="#tab5" aria-controls="about" aria-expanded="false"><i class="ft-file"></i> Advanced</a>
                                </li>
                            </ul>
                            <div class="tab-content px-1 pt-1">
                                <div role="tabpanel" class="tab-pane active" id="tab1" aria-labelledby="homeIcon-tab" aria-expanded="true">
                                    <br><br>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="width: 260px"><strong>{{__('company.company_name')}}</strong></td>
                                            <td>{{$data->name}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.legal_company_name')}}</strong></td>
                                            <td>{{$data->legal_name}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.company_holder')}}</strong></td>
                                            <td>{{$data->company_holder}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.industry')}}</strong></td>
                                            <td>{{$data->industry}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.type_of_company')}}</strong></td>
                                            <td>{{$data->type_of_company}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.address')}}</strong></td>
                                            <td>
                                                {{$data->street}},
                                                <br>{{$data->city}},
                                                <br>{{$data->zip_code}},
                                                <br>{{$data->country}}.
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="tab-pane" id="tab2" role="tabpanel" aria-labelledby="profileIcon-tab" aria-expanded="false">
                                    <br><br>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="width: 260px"><strong>{{__('company.district-court')}}</strong></td>
                                            <td>{{$data->district_court}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.corporation-registration-number')}}</strong></td>
                                            <td>{{$data->co_reg_number}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.sales-tax-id')}}</strong></td>
                                            <td>{{$data->sales_tax_id}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.tax-reference')}}</strong></td>
                                            <td>{{$data->tax_ref}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.tax-rate')}}</strong></td>
                                            <td>{{number_format($data->tax_rate,2)}}</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="tab-pane" id="tab3" role="tabpanel" aria-labelledby="aboutIcon-tab" aria-expanded="false">
                                    <br><br>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="width: 260px"><strong>{{__('company.phone')}}</strong></td>
                                            <td>{{$data->phone}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.fax')}}</strong></td>
                                            <td>{{$data->fax}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.email')}}</strong></td>
                                            <td>{{$data->email}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.web')}}</strong></td>
                                            <td>{{$data->web}}</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="tab-pane" id="tab4" role="tabpanel" aria-labelledby="aboutIcon-tab" aria-expanded="false">
                                    <br><br>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td style="width: 260px"><strong>{{__('company.bank')}}</strong></td>
                                            <td>{{$data->bank}}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>{{__('company.account-number')}}</strong></td>
                                            <td>{{$data->acount_number}}</td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="tab-pane" id="tab5" role="tabpanel" aria-labelledby="aboutIcon-tab" aria-expanded="false">
                                    <br><br>
                                    <a class="btn btn-primary btn-glow btn-sm" href="{{action('CompanyController@edit',$data->id)}}"><i class="fa fa-pencil"></i> Edit Company Profile</a>
                                </div>
                            </div>

                        </div> <!-- end of .card-body -->
                    </div>
                </div>
            </div>
            </section>
        </div>
      {{--  </div>   --}}
      {{--  end of the .content-right  --}}
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection

@section('scripts')
    <script src="{{ url('template-assets/js/scripts/pages/users-contacts.js') }}"></script>
@endsection
