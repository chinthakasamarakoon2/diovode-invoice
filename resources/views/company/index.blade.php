@extends('layouts.app')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
      <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
          <h3 class="content-header-title mb-0 d-inline-block">Companies</h3>

        </div>

          <div class="col-md-4 col-sm-12">
              <a href="{{url('companies/create')}}" class="btn btn-success btn-sm pull-right"><i class="ft-plus white"></i> Add Company</a>
          </div>
      </div>
      {{--  <div class="content-detached">  --}}

        <div class="content-body">
            <section class="row">
            <div class="col-12">

                <div class="card">

                    <div class="card-content">
                        <div class="card-body border-top-blue-grey border-top-lighten-5">

                            @include('includes.massage')
                            <!-- Task List table -->
                            <div class="table-responsive">
                                <table id="users-contacts" class="table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Fax</th>
                                            <th>Email</th>
                                            <th>Active</th>
                                            <th style="max-width: 100px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($companies as $company)
                                            <tr>
                                                <td>{{$company->id}}</td>
                                                <td class="text-left">{{$company->name}}</td>
                                                <td class="text-left">{{$company->phone}}</td>
                                                <td class="text-left">{{$company->fax}}</td>
                                                <td class="text-left">{{$company->email}}</td>
                                                <td class="text-center">
                                                    @if($company->active)
                                                        <span class="badge badge-success">Yes</span>
                                                    @else
                                                    <span class="badge badge-danger">No</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <div class="btn-group" role="group" aria-label="Basic example">
                                                        <a title="View Profile" href="{{action('CompanyController@show',$company->id)}}" class="btn btn-icon btn-info btn-sm">Profile</a>

                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
      {{--  </div>   --}}
      {{--  end of the .content-right  --}}
      </div>
    </div>
  </div>
  <!-- ////////////////////////////////////////////////////////////////////////////-->
@endsection

@section('scripts')
    <script src="{{ url('template-assets/js/scripts/pages/users-contacts.js') }}"></script>
@endsection
