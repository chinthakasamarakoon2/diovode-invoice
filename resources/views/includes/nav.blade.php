<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

        <li class="nav-item"><a href="#"><i class="icon-lock-open"></i><span class="menu-title" data-i18n="nav.login_register_pages.main">@lang('nav.access_control')</span></a>
          <ul class="menu-content">
            <li class="{{ Request::is('roles*') ? 'active' : '' }}"><a class="menu-item" href="{{url('roles')}}">@lang('nav.Roles')</a></li>
            <li class="{{ Request::is('role-permissions*') ? 'active' : '' }} d-none"><a class="menu-item" href="{{url('role-permissions')}}">@lang('nav.Role-Permissions')</a></li>
            <li class="{{ Request::is('users*') ? 'active' : '' }}"><a class="menu-item" href="{{url('users')}}" >@lang('nav.Users')</a></li>
          </ul>
        </li>

        <li class="nav-item"><a href="#"><i class="icon-lock-open"></i><span class="menu-title" data-i18n="nav.login_register_pages.main">@lang('nav.settings')</span></a>
            <ul class="menu-content">
              <li class="{{ Request::is('companies*') ? 'active' : '' }}"><a class="menu-item" href="{{url('companies')}}">@lang('nav.companies')</a></li>
            </ul>
          </li>

      </ul>
    </div>
  </div>
