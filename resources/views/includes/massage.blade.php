<div class="row">
    @if(session('success'))

    <div class="alert alert-success alert-dismissible mb-2 col-md-12" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>{{session('success')}}</strong>
    </div>

    @endif
    @if($errors->any())
    <div  id="error_msg" class="alert alert-dismissible alert-danger col-md-12" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    	<strong>
            @if($errors->any())
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            @endif
        </strong><a id="remove_err" href=""><i class="md md-close pull-right"></i></a>
    </div>
    @endif
    @if(session('error'))
    <div id="error_msg" class="alert alert-danger alert-dismissible mb-2 col-md-12" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    	<strong>{{session('error')}}</strong>
    </div>
    @endif
</div>
